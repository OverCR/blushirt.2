﻿using System;
using System.Text;

namespace BLUSHIRT._2 {
    class BlueCryptEngine {
        // Leading file marker.
        const string marker = "BLUARCH.V2\x0\x0\x1";
        // Trailing file marker.
        const string endMarker = "\x0\x1\x33ARC_RVN\x0\x0";

        /// <summary>
        /// Creates a new BlueCrypt Engine object.
        /// </summary>
        public BlueCryptEngine() {

        }

        /// <summary>
        /// Encrypts any data with Blueshirt 2.
        /// </summary>
        /// <param name="data">Any data byte array to encrypt.</param>
        /// <returns>Encrypted data or null if failed.</returns>
        public byte[] EncryptData(byte[] data) {
            byte[] toEncrypt = data;

            // Don't bother to do anything if array is null.
            if(toEncrypt == null) {
                return null;
            }

            // Initialize data containers to work with.
            byte[] markerData = Encoding.ASCII.GetBytes(marker);
            byte[] endMarkerData = Encoding.ASCII.GetBytes(endMarker);
            
            string base64EncData = Convert.ToBase64String(toEncrypt);
            byte[] base64ByteData = Encoding.Unicode.GetBytes(base64EncData);
            
            byte[] encryptedData = new byte[markerData.Length + base64ByteData.Length + endMarkerData.Length];
            
            // Copy marker to new array and make it a valid BLUSHIRT.2 container.
            markerData.CopyTo(encryptedData, 0);

            // Try to encrypt all data.
            try {
                for(int i = 0; i < base64ByteData.Length; i++) {
                    base64ByteData[i] = (byte)(base64ByteData[i] ^ 169);
                }
            } catch { 
                // Something's wrong? Bail out returning null;
                return null;
            }

            // Copy to final data container.
            Array.Copy(base64ByteData, 0, encryptedData, markerData.Length, base64ByteData.Length);
            
            // Add end marker.
            Array.Copy(endMarkerData, 0, encryptedData, markerData.Length + base64ByteData.Length, endMarkerData.Length);

            // Finally, return encrypted data to caller.
            return encryptedData;
        }

        /// <summary>
        /// Decrypts a Blueshirt 2 file byte array.
        /// </summary>
        /// <param name="data">Valid byte array to decrypt.</param>
        /// <returns>Decrypted data, or null if failed.</returns>
        public byte[] DecryptData(byte[] data) {
            // Prepare source data container.
            byte[] encryptedData = data;

            // Don't bother to decrypt any data if source is null.
            if(encryptedData == null) {
                return null;
            }

            // Prepare additional data containers.
            byte[] markerData = Encoding.ASCII.GetBytes(marker);
            byte[] endMarkerData = Encoding.ASCII.GetBytes(endMarker);
            byte[] decryptedData = new byte[data.Length - markerData.Length - endMarkerData.Length];

            // Copy the array, stripping first marker and the end marker.
            Array.Copy(encryptedData, marker.Length, decryptedData, 0, decryptedData.Length);


            // Try to decrypt all data.
            try {
                for(int i = 0; i < decryptedData.Length; i++) {
                    decryptedData[i] = (byte)(decryptedData[i] ^ 169);
                }
            } catch {
                // If there's something wrong, bail out returning null.
                return null;
            }
            byte[] base64DecData = null;

            // Break Base64 apart and recover original data.
            // Use Unicode encoding, so we don't lose binary data if encrypted.
            try {
                base64DecData = Convert.FromBase64String(Encoding.Unicode.GetString(decryptedData));
            } catch {
                return null;
            }

            // Finally, return decrypted data to a caller.
            return base64DecData;
        }
    }
}
