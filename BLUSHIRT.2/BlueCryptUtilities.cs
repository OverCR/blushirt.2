﻿using System;
using System.IO;
using System.Text;

namespace BLUSHIRT._2 {
    class BlueCryptUtilities {
        // Leading file marker.
        const string marker = "BLUARCH.V2\x0\x0\x1";
        // Trailing file marker.
        const string endMarker = "\x0\x1\x33ARC_RVN\x0\x0";

        /// <summary>
        /// Test if a file is a valid Blueshirt 2 archive.
        /// </summary>
        /// <param name="fileName">File path to test.</param>
        /// <returns>False if not valid archive.</returns>
        /// <returns>True if valid archive.</returns>
        public static bool IsValidBlueShirtArchive(string fileName) {
            if(File.Exists(fileName)) {
                try {
                    // Source data.
                    byte[] data = File.ReadAllBytes(fileName);
                    byte[] markerData = Encoding.ASCII.GetBytes(marker);
                    byte[] endMarkerData = Encoding.ASCII.GetBytes(endMarker);

                    // Container for data to compare.
                    byte[] encryptedDataMarker = new byte[markerData.Length];
                    byte[] encryptedDataEndMarker = new byte[endMarkerData.Length];

                    // Copy whatever the data is into containers.
                    Array.Copy(data, 0, encryptedDataMarker, 0, markerData.Length);
                    Array.Copy(data, (data.Length - endMarkerData.Length), encryptedDataEndMarker, 0, endMarkerData.Length);

                    // Test if the leading marker perfectly matches...
                    for(int i = 0; i < encryptedDataMarker.Length; i++) {
                        if(markerData[i] != encryptedDataMarker[i]) return false;
                    }

                    // Test if the trailing marker perfectly matches...
                    for(int i = 0; i < encryptedDataEndMarker.Length; i++) {
                        if(endMarkerData[i] != encryptedDataEndMarker[i]) return false;
                    }
                    return true;
                } catch {
                    return false;
                }
            }
            return false;
        }
    }
}
