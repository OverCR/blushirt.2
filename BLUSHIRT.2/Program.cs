﻿using System;
using System.Diagnostics;
using System.IO;

namespace BLUSHIRT._2 {
    class Program {
        static BlueCryptEngine BCE = new BlueCryptEngine();

        // TODO: Rewrite crappy argument parsing (How do I can even call that 'parsing'..?)
        static void Main(string[] args) {
            if(args.Length <= 0) {
                InvalidUsage();
            } else {
                if(args.Length == 1 || args[0] == "-h" || args[0] == "--help") InvalidUsage();
                if(args.Length == 2) {
                    if(args[0] == "-e" || args[0] == "--encrypt") {
                        if(File.Exists(args[1])) {
                            if(!BlueCryptUtilities.IsValidBlueShirtArchive(args[1])) {
                                byte[] data = File.ReadAllBytes(args[1]);
                                byte[] encryptedData = BCE.EncryptData(data);

                                try {
                                    File.Delete(args[1]);
                                    File.WriteAllBytes(args[1], encryptedData);
                                } catch {
                                    Console.WriteLine("Error writing encrypted data.");
                                    Environment.Exit(3);
                                }
                            } else {
                                Console.WriteLine("Input file already encrypted.");
                                Environment.Exit(1);
                            }
                        } else {
                            Console.WriteLine("Input file does not exist.");
                            Environment.Exit(2);
                        }
                    }

                    if(args[0] == "-d" || args[0] == "--decrypt") {
                        if(File.Exists(args[1])) {
                            if(BlueCryptUtilities.IsValidBlueShirtArchive(args[1])) {
                                byte[] encryptedData = File.ReadAllBytes(args[1]);
                                byte[] decryptedData = BCE.DecryptData(encryptedData);

                                try {
                                    File.Delete(args[1]);
                                    File.WriteAllBytes(args[1], decryptedData);
                                } catch {
                                    Console.WriteLine("Error writing decrypted data.");
                                    Environment.Exit(3);
                                }
                            } else {
                                Console.WriteLine("Invalid or corrupt archive.");
                                Environment.Exit(1);
                            }
                        } else {
                            Console.WriteLine("Input file does not exist.");
                            Environment.Exit(2);
                        }
                    }

                    if(args[0] == "-c" || args[0] == "--checkrc") {
                        if(File.Exists(args[1])) {
                            if(BlueCryptUtilities.IsValidBlueShirtArchive(args[1])) {
                                Console.WriteLine("This archive is a valid BlueShirt archive.\nUse '-d' switch to decrypt it.");
                                Environment.Exit(0);
                            } else {
                                Console.WriteLine("This archive is not a valid BlueShirt archive.\nIt is either corrupted or not encrypted at all.");
                                Environment.Exit(1);
                            }
                        } else {
                            Console.WriteLine("Input file does not exist.");
                            Environment.Exit(2);
                        }
                    }
                }

                if(args.Length == 3) {
                    if(args[0] == "-e" || args[0] == "--encrypt") {
                        if(File.Exists(args[1])) {
                            if(!BlueCryptUtilities.IsValidBlueShirtArchive(args[1])) {
                                byte[] data = File.ReadAllBytes(args[1]);
                                byte[] encryptedData = BCE.EncryptData(data);

                                try {
                                    File.WriteAllBytes(args[2], encryptedData);
                                } catch {
                                    Console.WriteLine("Error writing encrypted data.");
                                    Environment.Exit(3);
                                }
                            } else {
                                Console.WriteLine("Input file already encrypted.");
                                Environment.Exit(1);
                            }
                        } else {
                            Console.WriteLine("Input file does not exist.");
                            Environment.Exit(2);
                        }
                    }

                    if(args[0] == "-d" || args[0] == "--decrypt") {
                        if(File.Exists(args[1])) {
                            if(BlueCryptUtilities.IsValidBlueShirtArchive(args[1])) {
                                byte[] encryptedData = File.ReadAllBytes(args[1]);
                                byte[] decryptedData = BCE.DecryptData(encryptedData);

                                try {
                                    File.WriteAllBytes(args[2], decryptedData);
                                } catch {
                                    Console.WriteLine("Error writing decrypted data.");
                                    Environment.Exit(3);
                                }
                            } else {
                                Console.WriteLine("Invalid or corrupt archive.");
                                Environment.Exit(1);
                            }
                        } else {
                            Console.WriteLine("Input file does not exist.");
                            Environment.Exit(2);
                        }
                    }
                }
            } 
        }

        static void InvalidUsage() {
            Console.WriteLine("BLUESHIRT.2 command line utility.");
            Console.WriteLine("-------------------------------------------------");
            Console.WriteLine("Usage: '" + Process.GetCurrentProcess().ProcessName + " (-e|-d|-c|-h) <input_file> [output_file]'");
            Console.WriteLine("   -d --decrypt: Decrypts a BLUESHIRT.2 encrypted archive.");
            Console.WriteLine("   -e --encrypt: Encrypts a file with BLUESHIRT.2");
            Console.WriteLine("   -c --checkrc: Checks integrity of BLUESHIRT.2 archive.");
            Console.WriteLine("   -h --help: Displays this message.");
            Environment.Exit(0);
        }
    }
}
